package Ex1.Problem2;



//exercise 2 - 3 types Implementation of HashCode

public class GoodHashCode {
	int number;
	public GoodHashCode(int number) {
		this.number=number;
	}
	@Override
	public int hashCode() {
		return number;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof GoodHashCode))
			return false;
		GoodHashCode newObject = (GoodHashCode) obj;
		return number == newObject.number;
	}
}
