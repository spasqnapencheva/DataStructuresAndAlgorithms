package Ex1.Problem2;



//exercise 2 - 3 types Implementation of HashCode

public class BadHashCode {
	int number;
	public BadHashCode(int number) {
		this.number=number;
	}
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof BadHashCode)) return false;
		BadHashCode newObject=(BadHashCode)obj;
		return number==newObject.number;
	}
	@Override
	public int hashCode() {
		return 1;
	}
}
