package Ex1.Problem2;

import java.util.HashMap;


//exercise 2 - 3 types Implementation of HashCode

public class HashCodeImpl {

	public static void main(String[] args) {
		HashMap<GoodHashCode, Integer> goodHashMap=new HashMap<>();
		HashMap<MiddleHashCode, Integer> middleHashMap=new HashMap<>();
		HashMap<BadHashCode, Integer> badHashMap=new HashMap<>();
		
		long time=System.nanoTime();
		for (int i = 0; i <= 10000; i++) {
			badHashMap.put(new BadHashCode(i), i);
		}
		
		long newTime=System.nanoTime() - time;
		System.out.println("Time of bad implementation: "+ newTime/ 1_000_000_000.0 + " sec");
		
		time=System.nanoTime();
		for (int i = 0; i <= 10000; i++) {
			middleHashMap.put(new MiddleHashCode(i), i);
		}
		newTime=System.nanoTime() - time;
		System.out.println("Time of middle type implementation: "+ newTime/ 1_000_000_000.0 + " sec");
		time=System.nanoTime();
		
		for (int i = 0; i <= 10000; i++) {
			goodHashMap.put(new GoodHashCode(i), i);
		}
		newTime=System.nanoTime() - time;
		System.out.println("Time of good implementation: "+ newTime/ 1_000_000_000.0 + " sec");
		time=System.nanoTime();
	}

}
