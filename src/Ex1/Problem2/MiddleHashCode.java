package Ex1.Problem2;



//exercise 2 - 3 types Implementation of HashCode

public class MiddleHashCode {

	int number;
	
	public MiddleHashCode(int number) {
		this.number=number;
	}
	@Override
	public int hashCode() {
		int sum = 0;
		for (int i = 0; i < 10; i++) {
			sum += i;
		}
		return sum;

	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof MiddleHashCode))
			return false;
		MiddleHashCode newObject = (MiddleHashCode) obj;
		return number == newObject.number;
	}

}
