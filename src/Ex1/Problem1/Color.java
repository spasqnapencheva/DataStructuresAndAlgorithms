package Ex1.Problem1;
//Exercise 1 - ColorPicker

import java.util.HashMap;
import java.util.Scanner;
import java.util.Map.Entry;

public class Color {
	public static HashMap<String, String> colors = new HashMap<>();
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		colors.put("red", "#cf3523");
		colors.put("orange", "#ff6633");
		colors.put("yellow", "#ffd700");
		colors.put("green", "#337465");
		colors.put("blue", "#0078ff");
		colors.put("purple", "#110055");
		colors.put("pink", "#ffb6c1");
		colors.put("dark red", "#521515");
		colors.put("brown", "#100404");
		colors.put("black", "#000000");
		colors.put("white", "#FFFFFF");
		colors.put("grey", "#808080");
		colors.put("light blue", "#cedee9");
		colors.put("light green", "#d3ffce");
		colors.put("magenta", "#FF00FF");

		int counter = 0;
		for (Entry<String, String> entry : colors.entrySet()) {
			++counter;
			System.out.println("[" + counter + "]" + ". Color: " + entry.getKey());
		}
		pickColor();
		scanner.close();
	}
	
	public static void pickColor() {
		System.out.println("Choose your color to take the hex code:");
		String choice = scanner.nextLine();
		switch (choice) {
		case "1":
			System.out.println("Color:Red    Hex Code: " + colors.get("red"));
			break;
		case "2":
			System.out.println("Color:Orange    Hex Code: " + colors.get("orange"));
			break;
		case "3":
			System.out.println("Color:Yellow    Hex Code: " + colors.get("yellow"));
			break;
		case "4":
			System.out.println("Color:Green    Hex Code: " + colors.get("green"));
			break;
		case "5":
			System.out.println("Color:Blue    Hex Code: " + colors.get("blue"));
			break;
		case "6":
			System.out.println("Color:Purple    Hex Code: " + colors.get("purple"));
			break;
		case "7":
			System.out.println("Color:Pink    Hex Code: " + colors.get("pink"));
			break;
		case "8":
			System.out.println("Color:Dark Red    Hex Code: " + colors.get("dark red"));
			break;
		case "9":
			System.out.println("Color:Brown    Hex Code: " + colors.get("brown"));
			break;
		case "10":
			System.out.println("Color:Black    Hex Code: " + colors.get("black"));
			break;
		case "11":
			System.out.println("Color:White    Hex Code: " + colors.get("white"));
			break;
		case "12":
			System.out.println("Color:Grey    Hex Code: " + colors.get("grey"));
			break;
		case "13":
			System.out.println("Color:Light Blue    Hex Code: " + colors.get("light blue"));
			break;
		case "14":
			System.out.println("Color:Light Green    Hex Code: " + colors.get("light green"));
			break;
		case "15":
			System.out.println("Color:Magenta   Hex Code: " + colors.get("magenta"));
			break;
		default:
			System.out.println("Please, enter your choice CORRECTLY!");
			pickColor();

		}
	}

}