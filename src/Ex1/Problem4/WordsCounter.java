package Ex1.Problem4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class WordsCounter {

	public static void main(String[] args) throws IOException {

		System.out.println("Counting Words");

		List<String> words = new ArrayList<>();
		FileReader fr = new FileReader("text.txt");
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();
		line = line.toLowerCase();

		while (line != null) {
			line = replaceSymbols(line);
			String[] parts = line.split("[ ,\\.]");

			for (String part : parts) {
				if (!(part.equals(""))) {
					words.add(part);
				}

			}
			line = br.readLine();
		}

		Map<String, Integer> wordOcc = new HashMap<>();

		for (String word : words) {
			if (!wordOcc.containsKey(word)) {
				wordOcc.put(word, 1);
			} else {
				int counter = wordOcc.get(word);
				wordOcc.put(word, ++counter);
			}

		}

		for (Entry<String, Integer> value : wordOcc.entrySet()) {
			System.out.println(value.getKey() + " : " + value.getValue());
		}
		br.close();

	}

	public static String replaceSymbols(String line) {
		line = line.replace('"', ',');
		line = line.replace('?', ',');
		line = line.replace('!', ',');
		line = line.replace('[', ',');
		line = line.replace(']', ',');
		line = line.replace('(', ',');
		line = line.replace(')', ',');
		line = line.replace('-', ',');
		line = line.replace('*', ',');
		line = line.replace('+', ',');
		line = line.replace('^', ',');
		line = line.replace('%', ',');
		line = line.replace('@', ',');
		line = line.replace('~', ',');
		line = line.replace('`', ',');
		line = line.replace('#', ',');
		line = line.replace('$', ',');
		line = line.replace('&', ',');
		line = line.replace('_', ',');
		line = line.replace('=', ',');
		line = line.replace(':', ',');
		line = line.replace('/', ',');
		line = line.replace('„', ',');
		line = line.replace('“', ',');
		line = line.replace('§', ',');
		line = line.replace('€', ',');
		line = line.replace('№', ',');
		line = line.replace('}', ',');
		line = line.replace('{', ',');
		return line;

	}

}
