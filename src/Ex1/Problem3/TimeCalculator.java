package Ex1.Problem3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TimeCalculator {
	public static ConcurrentHashMap<Integer, Integer> firstConHashMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, Integer> secondConHashMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, Integer> thirdConHashMap = new ConcurrentHashMap<>();

	class FirstConHashMap implements Runnable {
		@Override
		public void run() {
			for (int i = 0; i < 10_000; i++) {
				firstConHashMap.put(i, new Random().nextInt(20_000));
			}
		}
	}

	class SecondConHashMap implements Runnable {
		@Override
		public void run() {
			for (int i = 0; i < 100_000; i++) {
				secondConHashMap.put(i, new Random().nextInt(200_000));

			}
		}
	}

	class ThirdConHashMap implements Runnable {
		@Override
		public void run() {
			for (int i = 0; i < 1_000_000; i++) {
				thirdConHashMap.put(i, new Random().nextInt(2_000_000));
			}
		}
	}

	public static void main(String[] args) {
		HashMap<Integer, Integer> firstHashMap = new HashMap<>();
		HashMap<Integer, Integer> secondHashMap = new HashMap<>();
		HashMap<Integer, Integer> thirdHashMap = new HashMap<>();
		TimeCalculator tCalculator = new TimeCalculator();
		ExecutorService service = Executors.newFixedThreadPool(3);

		long time = System.nanoTime();

		for (int i = 0; i < 10000; i++) {
			firstHashMap.put(i, new Random().nextInt(2000));
		}

		long newTime = System.nanoTime() - time;
		System.out.println("HashMap time with 10 000 : " + newTime / 1000000.0 + " ms");

		time = System.nanoTime();

		for (int i = 0; i < 100000; i++) {
			secondHashMap.put(i, new Random().nextInt(20000));
		}
		newTime = System.nanoTime() - time;
		System.out.println("HashMap time with 100 000 : " + newTime / 1000000.0 + " ms");

		time = System.nanoTime();
		for (int i = 0; i < 1000000; i++) {
			thirdHashMap.put(i, new Random().nextInt(200000));
		}
		newTime = System.nanoTime() - time;
		System.out.println("HashMap time with 1 000 000 : " + newTime / 1000000.0 + " ms");

		time = System.nanoTime();

		service.execute(tCalculator.new FirstConHashMap());
		service.execute(tCalculator.new FirstConHashMap());
		service.execute(tCalculator.new FirstConHashMap());

		try {
			while (firstConHashMap.size() < 10_000) {
				Thread.sleep(20);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		newTime = System.nanoTime() - time;
		System.out.println("ConcurrentHashMap time with 10 000 : " + newTime / 1000000.0 + " ms");

		time = System.nanoTime();
		service.execute(tCalculator.new SecondConHashMap());
		service.execute(tCalculator.new SecondConHashMap());
		service.execute(tCalculator.new SecondConHashMap());
		try {
			while (secondConHashMap.size() < 10_000) {
				Thread.sleep(20);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		newTime = System.nanoTime() - time;
		System.out.println("ConcurrentHashMap time with 100 000 : " + newTime / 1000000.0 + " ms");

		time = System.nanoTime();
		service.execute(tCalculator.new ThirdConHashMap());
		service.execute(tCalculator.new ThirdConHashMap());
		service.execute(tCalculator.new ThirdConHashMap());
		try {
			while (thirdConHashMap.size() < 10_000) {
				Thread.sleep(20);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		newTime = System.nanoTime() - time;
		System.out.println("ConcurrentHashMap time with 1 000 000 : " + newTime / 1000000.0 + " ms");

		time = System.nanoTime();
		searchDigitsAsKeyAndValue(firstHashMap);
		newTime = System.nanoTime() - time;
		System.out.println("Print HashMap's digits : " + newTime / 1000000.0 + " ms");
		time = System.nanoTime();
		searchDigitsAsKeyAndValue(firstConHashMap);
		newTime = System.nanoTime() - time;
		System.out.println("Print ConHashMap's digits : " + newTime / 1000000.0 + " ms");

	}

	public static void searchDigitsAsKeyAndValue(Map<Integer, Integer> map) {
		List<Integer> numbers = new ArrayList<>();
		for (Integer key : map.keySet()) {
			if (map.containsValue(key)) {
				numbers.add(key);
			}
		}

		for (Integer number : numbers) {
			System.out.println(number);
		}
	}

}
