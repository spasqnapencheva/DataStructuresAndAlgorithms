package Ex3.Problem3;

public class QuickSelect {
	public static int findKthLargest(int[] nums, int k) {
		int start = 0, end = nums.length - 1, index = nums.length - k;

		while (start < end) {
			int pivot = partion(nums, start, end);

			if (pivot < index)
				start = pivot + 1;
			else if (pivot > index)
				end = pivot - 1;
			else
				return nums[pivot];
		}
		return nums[start];
	}

	private static int partion(int[] nums, int start, int end) {
		int pivot = start;
		int temp;

		while (start <= end) {
			while (start <= end && nums[start] <= nums[pivot])
				start++;
			while (start <= end && nums[end] > nums[pivot])
				end--;

			if (start > end)
				break;

			temp = nums[start];
			nums[start] = nums[end];
			nums[end] = temp;
		}

		temp = nums[end];
		nums[end] = nums[pivot];
		nums[pivot] = temp;

		return end;
	}

	public void selectAndPrint(int[] arr, int start, int end, int step) {
		System.out.println();
		System.out.println("Elements: " + start + " - " + end);
		System.out.println("***************************");

		long time;
		long newTime;
		for (int i = start; i <= end; i += step) {

			if (i == start) {
				time = System.nanoTime();
				System.out.println(i + " element: " + findKthLargest(arr, start));
				newTime = System.nanoTime() - time;
				System.out.println("Time of finding " + i + " element: " + newTime / 1000000.0 + " ms");

			} else {
				System.out.println(i + " element: " + findKthLargest(arr, i));
			}
		}
	}
}
