package Ex3.Problem3;

import java.util.Random;

public class Main {
	static Random random = new Random();

	public static void main(String[] args) {
		int[] arr = new int[1_000_001];
		int[] copyArr=new int[1_000_001];
		
		for (int i = 0; i < 1_000_000; i++) {
			arr[i] = random.nextInt(100_000);
		}
		
		for (int i = 0; i < arr.length; i++) {
			copyArr[i]=arr[i];
		}
		QuickSelect quickSelect = new QuickSelect();
		
		quickSelect.selectAndPrint(copyArr, 10_000, 50_000, 10_000);
		
		for (int i = 0; i < arr.length; i++) {
			copyArr[i]=arr[i];
		}
		quickSelect.selectAndPrint(copyArr, 50_000, 500_000, 50_000);
		
		for (int i = 0; i < arr.length; i++) {
			copyArr[i]=arr[i];
		}
		quickSelect.selectAndPrint(copyArr, 500_000, 1_000_000, 100_000);

	}

}
