package Ex3.Problem2;

import java.util.Random;

import Ex3.Problem1.QuickSort;

public class Timer {
	public static Random random=new Random();
	public static void main(String[] args) {
		int[] mergeArr = new int[100_000];
		int[] quickArr = new int[100_000];
		int[] bubbleArr = new int[100_000];
		for (int i = 0; i < 100_000; i++) {
			int number=random.nextInt(100);
			mergeArr[i]=number;
			quickArr[i]=number;
			bubbleArr[i]=number;
		}
		MergeSort mergeSort=new MergeSort();
		QuickSort quickSort=new QuickSort();
		BubbleSort bubbleSort=new BubbleSort();
		
		long time=System.nanoTime();
		mergeSort.sort(mergeArr);
		long newTime=System.nanoTime() - time;
		System.out.println("Time of MergeSort: "+ newTime/ 1_000_000_000.0 + " sec");
		
		time=System.nanoTime();
		quickSort.sort(quickArr);
		newTime=System.nanoTime() - time;
		System.out.println("Time of QuickSort: "+ newTime/ 1_000_000_000.0 + " sec");
		
		time=System.nanoTime();
		bubbleSort.sort(bubbleArr);
		newTime=System.nanoTime() - time;
		System.out.println("Time of BubbleSort: "+ newTime/ 1_000_000_000.0 + " sec");
		
		
		

	}
}
