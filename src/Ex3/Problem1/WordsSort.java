package Ex3.Problem1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

public class WordsSort {

	public static void main(String[] args) throws IOException {

		System.out.println("Counting Words");
		System.out.println("**********************");
		QuickSort quickSort = new QuickSort();
		HashMap<String, Integer> words = new HashMap<>();
		FileReader fr = new FileReader("text.txt");
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();
		line = line.toLowerCase();

		while (line != null) {
			line = replaceSymbols(line);
			String[] parts = line.split("[ ,\\.]");

			for (int i = 0; i < 201; i++) {
				if (!(parts[i].equals(""))) {
					words.put(parts[i], parts[i].length());
				}
			}
			line = br.readLine();
		}

		int[] chCount = new int[words.size()];
		int counter = 0;
		for (Integer value : words.values()) {
			chCount[counter++] = value;
		}
		quickSort.sort(chCount);
		for (int i = 0; i < chCount.length; i++) {
			for (Entry<String, Integer> value : words.entrySet()) {
				if (value.getValue() == chCount[i]) {
					System.out.println(value.getKey() + ": " + value.getValue() + " ltrs");
				}
			}

		}

		br.close();

	}

	public static String replaceSymbols(String line) {
		line = line.replace('"', ',');
		line = line.replace('?', ',');
		line = line.replace('!', ',');
		line = line.replace('[', ',');
		line = line.replace(']', ',');
		line = line.replace('(', ',');
		line = line.replace(')', ',');
		line = line.replace('-', ',');
		line = line.replace('*', ',');
		line = line.replace('+', ',');
		line = line.replace('^', ',');
		line = line.replace('%', ',');
		line = line.replace('@', ',');
		line = line.replace('~', ',');
		line = line.replace('`', ',');
		line = line.replace('#', ',');
		line = line.replace('$', ',');
		line = line.replace('&', ',');
		line = line.replace('_', ',');
		line = line.replace('=', ',');
		line = line.replace(':', ',');
		line = line.replace('/', ',');
		line = line.replace('„', ',');
		line = line.replace('“', ',');
		line = line.replace('§', ',');
		line = line.replace('€', ',');
		line = line.replace('№', ',');
		line = line.replace('}', ',');
		line = line.replace('{', ',');

		return line;

	}

}
