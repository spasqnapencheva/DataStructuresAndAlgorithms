package Ex2.Problem2;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
	public static ConcurrentSkipListMap<ComparableImpl, Integer> listMap = new ConcurrentSkipListMap<>();
	static boolean firstCheck = false;
	Set<Integer> oddNumbers = new HashSet<>();
	Set<Integer> evenNumbers = new HashSet<>();

	class MapFiller implements Runnable {

		@Override
		public void run() {
			for (int i = 0; i < 1_000_000; i++) {
				listMap.put(new ComparableImpl(new Random().nextInt(2_000_000)), i);
			}
			firstCheck = true;

		}

	}

	class EvenValuesReader implements Runnable {

		@Override
		public void run() {

			for (Entry<ComparableImpl, Integer> entry : listMap.entrySet()) {
				if (entry.getValue() % 2 == 0) {
					evenNumbers.add(entry.getValue());
				}
			}

		}

	}

	class OddKeyNumberReader implements Runnable {

		@Override
		public void run() {
			for (Entry<ComparableImpl, Integer> entry : listMap.entrySet()) {
				if (entry.getKey().getNumber() % 2 != 0) {
					oddNumbers.add(entry.getKey().getNumber());
				}
			}
		}
	}

	public static void main(String[] args) {
		Main main = new Main();
		ExecutorService service = Executors.newFixedThreadPool(3);
		service.execute(main.new MapFiller());

		try {
			while (firstCheck == false) {
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		service.execute(main.new EvenValuesReader());
		service.execute(main.new OddKeyNumberReader());
		service.shutdown();

	}
}