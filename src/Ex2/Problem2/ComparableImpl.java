package Ex2.Problem2;

public class ComparableImpl implements Comparable<ComparableImpl> {
	int number;
	String name;
	String address;

	public ComparableImpl(int number) {
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + number;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj instanceof ComparableImpl)
			return false;
		ComparableImpl other = (ComparableImpl) obj;
		if (number != other.number)
			return false;
		return true;
	}

	@Override
	public int compareTo(ComparableImpl o) {
		if (o.number < this.number)
			return -1;
		if (o.number > this.number)
			return 1;

		return 0;
	}

}
