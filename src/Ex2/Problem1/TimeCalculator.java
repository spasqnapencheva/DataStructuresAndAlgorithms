package Ex2.Problem1;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;



public class TimeCalculator {
	public static Random rand=new Random();
	public static void main(String[] args) {
		TreeMap<Integer,Integer> treeMap=new TreeMap<>();
		HashMap<Integer, Integer> hashMap=new HashMap<>();
		System.out.println("TreeMap's time : "+ fillMap(treeMap)/ 1_000_000_000.0 + " sec");
		System.out.println("HashMap's time : "+ fillMap(hashMap)/ 1_000_000_000.0 + " sec");
		
		
	}
	
	public static long fillMap(Map<Integer, Integer> map) {
		long time=System.nanoTime();
		for (int i = 0; i <= 1_000_000; i++) {
			map.put(rand.nextInt(2_000_001), rand.nextInt(2_000_001));
		}
		
		long newTime=System.nanoTime() - time;
		return newTime;
	}
}
