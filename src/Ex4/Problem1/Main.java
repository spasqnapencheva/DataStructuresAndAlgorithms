package Ex4.Problem1;

public class Main {

	public static void main(String[] args) {

		Graph graph = new Graph(10);
		graph.addEdge(0, 1);
		graph.addEdge(1, 2);
		graph.addEdge(1, 3);
		graph.addEdge(3, 4);
		graph.addEdge(3, 7);
		graph.addEdge(4, 5);
		graph.addEdge(4, 6);
		graph.addEdge(4, 7);
		graph.addEdge(6, 9);
		graph.addEdge(7, 8);
		graph.addEdge(8, 9);

		System.out.println(graph.toString());
		new DepthFirstTraversal().traverse(graph, 2, 5);

	}

}
