package Ex4.Problem3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestDijkstra {

	private List<Vertex> nodes;
	private List<Edge> edges = new ArrayList<Edge>();
	public static boolean isWeight;

	public void createNodes(int countNodes) {
		nodes = new ArrayList<Vertex>();
		for (int i = 0; i < countNodes; i++) {
			Vertex location = new Vertex("Node_" + i, "Node_" + i);
			nodes.add(location);
		}
	}

	public void testExcuteByWeight(int start, int end) {
		isWeight = true;
		testExcute(start, end);

	}

	public void testExcuteByPrice(int start, int end) {
		isWeight = false;
		testExcute(start, end);
		
	}
	public void testExcute(int start, int end) {
		try {
			Graph graph = new Graph(nodes, edges);
			DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
			dijkstra.execute(nodes.get(start));
			LinkedList<Vertex> path = dijkstra.getPath(nodes.get(end));

			int counter = 0;

			for (Vertex vertex : path) {
				System.out.println(vertex);
				counter++;
			}
			System.out.println("Count: " + counter);

			int sum = 0;
			if (isWeight) {
				for (Edge edge : edges) {
					for (int i = 0; i < path.size() - 1; i++) {
						if (edge.getSource().getName().equals(path.get(i).getName())
								& edge.getDestination().getName().equals(path.get(i + 1).getName())) {
							sum += edge.getWeight();
						}
					}
				}
				System.out.println("Weight: " + sum);
			} else {
				for (Edge edge : edges) {
					for (int i = 0; i < path.size() - 1; i++) {
						if (edge.getSource().getName().equals(path.get(i).getName())
								& edge.getDestination().getName().equals(path.get(i + 1).getName())) {
							sum += edge.getPrice();
						}
					}
				}
				System.out.println("Cost: " + sum);
			}
		} catch (NullPointerException e) {
			System.out.println("No path.Check your edges!");
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Check graph's count! ");
		}

	}


	public void addLane(String laneId, int sourceLocNo, int destLocNo, int weight, int price) {
		Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), weight, price);
		edges.add(lane);
	}
}