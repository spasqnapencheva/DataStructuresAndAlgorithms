package Ex4.Problem3;

public class Main {

	public static void main(String[] args) {
		TestDijkstra testDijkstra=new TestDijkstra();
		testDijkstra.createNodes(11);
		testDijkstra.addLane("Edge_0", 0, 1, 85,15);
		testDijkstra.addLane("Edge_1", 0, 2, 217,30);
		testDijkstra.addLane("Edge_2", 0, 4, 173,45);
		testDijkstra.addLane("Edge_3", 2, 6, 186,32);
		testDijkstra.addLane("Edge_4", 2, 7, 103,56);
		testDijkstra.addLane("Edge_5", 3, 7, 183,46);
		testDijkstra.addLane("Edge_6", 5, 8, 250,67);
		testDijkstra.addLane("Edge_7", 8, 9, 84,12);
		testDijkstra.addLane("Edge_8", 7, 9, 167,34);
		testDijkstra.addLane("Edge_9", 4, 9, 502,65);
		testDijkstra.addLane("Edge_10", 9, 10, 40,122);
		testDijkstra.addLane("Edge_11", 1, 10, 600,98);

		testDijkstra.testExcuteByWeight(0, 9);
		testDijkstra.testExcuteByPrice(0, 9);


	}

}
