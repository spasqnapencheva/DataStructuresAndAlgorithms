package Ex4.Problem4;

class Node {
	int id;
	Node left;
	Node right;

	public Node(int id) {
		this.id = id;
		left = null;
		right = null;
	}
}
