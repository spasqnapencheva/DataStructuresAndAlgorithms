package Ex4.Problem2;

public class Main {

	public static void main(String[] args) {
		BreadthFirstSearch breadthFirstSearch = new BreadthFirstSearch(10);
		breadthFirstSearch.addEdge(0, 1);
		breadthFirstSearch.addEdge(1, 2);
		breadthFirstSearch.addEdge(1, 3);
		breadthFirstSearch.addEdge(3, 4);
		breadthFirstSearch.addEdge(3, 7);
		breadthFirstSearch.addEdge(4, 5);
		breadthFirstSearch.addEdge(4, 6);
		breadthFirstSearch.addEdge(4, 7);
		breadthFirstSearch.addEdge(6, 9);
		breadthFirstSearch.addEdge(7, 8);
		breadthFirstSearch.addEdge(8, 9);
		
		breadthFirstSearch.traverse(4,7);
	}

}
